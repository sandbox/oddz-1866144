<?php
/**
 * Ad repository
 */
class YahooApt_Ad_Repository implements DrupalEntityControllerInterface {
	
	public function __construct($entityType) {
		
	}
	
	public function resetCache(array $ids = null) {
		
	}
	
	/**
	 * @todo:
	 * 
	 * cache by id
	 * cache by names
	 */
	public function load($ids = array(), $conditions = array()) {
		
		// drupal_set_message('ad load');
		
		$query = $this->_makeBaseSelectQuery($conditions);
		
		$query->addTag('yahoo_apt_ad_load');
		
		$this->_includeCustomContentCategories($query,$conditions);
		$this->_includeReportingTags($query,$conditions);
		$this->_includeSizes($query,$conditions);
		
		if(!empty($ids)) {
			$query->condition('a.id',$ids);
		}
		
		/**
		 * Tag query
		 */
		//$query->addTag('yahoo_apt_ad_load');
		
		$result = $query->execute();
		
		$ads = $this->_mapSelectResult($result);
		
		return $ads;
		
	}
	
	/**
	 * Save ad to persistent storage mechanism.
	 * 
	 * @param YahooApt_Ad
	 */
	public function persist(YahooApt_Ad $ad) {
		
		$transaction = db_transaction();
		
		try {
			
			$data = array(
				'name'=> $ad->getName(),
				'delivery_mode'=> $ad->getDeliveryMode(),
				'marker'=> $ad->getMarker(),
				'redirect_click_wrapper'=> $ad->getRedirectClickWrapper()
			);
		
			if($ad->getId() !== null) {
				
				db_update('yahoo_apt_ad')
				->fields($data)
				->condition('id',$ad->getId())
				->execute();
				
			} else {
				
				$id = db_insert('yahoo_apt_ad')
				->fields($data)
				->execute();
				
				$ad->setId($id);
				
			}
				
			$this->_persistCustomContentCategories($ad);
			$this->_persistReportingTags($ad);
			$this->_persistSizes($ad);
			
		} catch(Exception $e) {
			$transaction->rollback();
			throw $e;			
		}
		
	}
	
	/**
	 * Physically remove ads and dependencies from database.
	 * 
	 * @param array ad ids
	 */
	public function purgeById($ids) {
		
		if(empty($ids)) {
			return;
		}
		
		/**
		 * Wrap everything in a transaction
		 */
		$transaction = db_transaction();
		
		try {
			
			/**
			 * Delete from tables dependent on ad.
			 */
			foreach(array('custom_content_category','reporting_tag','size') as $name) {
				$query = db_delete("yahoo_apt_ad_$name");
				$query->condition('ad_id',$ids);
				$query->execute();
			}
			
			/**
			 * Delete ads from mappings
			 */
			$query = db_delete('yahoo_apt_mapping_ad');
			$query->condition('ad_id',$ids);
			$query->execute();
		
			/**
			 * Delete the ad itself
			 */
			$query = db_delete('yahoo_apt_ad');
			$query->condition('id',$ids);
			return $query->execute();
			
		} catch(Exception $e) {
			$transaction->rollback();
			throw $e;	
		}
		
	}
	
	/**
	 * Create base select query.
	 */
	protected function _makeBaseSelectQuery($conditions) {	
		
		$query = db_select('yahoo_apt_ad','a');	
		$query->fields('a');
		
		if(isset($conditions['ad'])) {
			foreach($conditions['ad'] as $field=>$value) {
				if(strpos($field,'.') === false) {
					$query->condition("a.$field",$value);
				} else {
					$query->condition($field,$value);
				}
			}
		}
		
		return $query;
		
	}
	
	protected function _includeCustomContentCategories($query,$conditions) {		
		$query->leftJoin('yahoo_apt_ad_custom_content_category','c','a.id = %alias.ad_id');	
		$query->leftJoin('taxonomy_term_data','ct','c.term_id = %alias.tid');
		
		$query->addField('c','id','custom_content_category_id');
		$query->addField('ct','tid','custom_content_category_term_id');
		$query->addField('ct','name','custom_content_category_term_name');		
	}
	
	protected function _includeReportingTags($query,$conditions) {		
		$query->leftJoin('yahoo_apt_ad_reporting_tag','r','a.id = %alias.ad_id');	
		$query->leftJoin('taxonomy_term_data','rt','r.term_id = %alias.tid');
		
		$query->addField('r','id','reporting_tag_id');
		$query->addField('rt','tid','reporting_tag_term_id');
		$query->addField('rt','name','reporting_tag_term_name');

	}
	
	protected function _includeSizes($query,$conditions) {		
		$query->leftJoin('yahoo_apt_ad_size','s','a.id = %alias.ad_id');		
		$query->addField('s','id','ad_size_id');
		$query->addField('s','width','ad_size_width');
		$query->addField('s','height','ad_size_height');
		$query->addField('s','weight','ad_size_weight');
		$query->addField('s','priority','ad_size_priority');		
	}
	
	protected function _mapSelectResult($result) {
		
		/**
		 * Map raw data to domain object and create collection of ads.
		 */
		$ads = array();
		
		while($row=$result->fetchAssoc()) {
			
			if(!isset($ads[$row['id']])) {
				$ad = new YahooApt_ad();
				
				$ad->setId($row['id']);
				$ad->setName($row['name']);
				$ad->setDeliveryMode($row['delivery_mode']);
				$ad->setMarker($row['marker']);
				$ad->setRedirectClickWrapper($row['redirect_click_wrapper']);
			} else {
				$ad = $ads[$row['id']];
			}
			
			/**
			 * Associative property mappings
			 */
			$assoc = array(
				'reporting_tag'=>'ReportingTag',
				'custom_content_category'=> 'CustomContentCategory',
				'ad_size'=> 'Size'
			);
			
			/**
			 * Collect custom sizes, content categories and reporting tags.
			 */
			foreach($assoc as $name=>$base) {
				
				if(isset($row["{$name}_id"]) && !$ad->{"has$base"}($row["{$name}_id"])) {
					
					if(strcmp($name,'ad_size') === 0) {
						$ad->{"add$base"}((object) array(
							'id'=> $row["{$name}_id"],
							'width'=> $row["{$name}_width"],
							'height'=> $row["{$name}_height"],
							'weight'=> $row["{$name}_weight"],
							'priority'=> $row["{$name}_priority"]
						));
					} else {
						$ad->{"add$base"}((object) array(
							'id'=> $row["{$name}_id"],
							'term'=> (object) array(
								'id'=> $row["{$name}_term_id"],
								'name'=> $row["{$name}_term_name"]
							)
						));					
					}
					
				}
				
			}
			
			// @todo sort sizes. It will be done outside the query for optimization reasons
			
			if(!isset($ads[$row['id']])) {
				$ads[$row['id']] = $ad;
			}
			
		}
		
		return $ads;
		
	}
	
	protected function _persistCustomContentCategories(YahooApt_Ad $ad) {
		
		$insert = null;
		
		// Remove all existing custom content categories
		$delete = db_delete('yahoo_apt_ad_custom_content_category');
		$delete->condition('ad_id',$ad->getId());
		$delete->execute();
		
		foreach($ad->getCustomContentCategories() as $item) {
			
			$data = array(
				'term_id'=> $item->term->id
			);
				
			if($insert === null) {
				$insert = db_insert('yahoo_apt_ad_custom_content_category');
				$insert->fields(array('ad_id','term_id'));
			}
				
			$data['ad_id'] = $ad->getId();
				
			$insert->values($data);
		}
		
		if($insert !== null) {
			$insert->execute();	
		}
		
	}
	
	protected function _persistReportingTags(YahooApt_Ad $ad) {
		
		$insert = null;
		
		// remove all existing reporting tags
		$delete = db_delete('yahoo_apt_ad_reporting_tag');
		$delete->condition('ad_id',$ad->getId());
		$delete->execute();
		
		foreach($ad->getReportingTags() as $item) {
			
			$data = array(
				'term_id'=> $item->term->id
			);
				
			if($insert === null) {
				$insert = db_insert('yahoo_apt_ad_reporting_tag');
				$insert->fields(array('ad_id','term_id'));
			}
				
			$data['ad_id'] = $ad->getId();			
			$insert->values($data);
		}
		
		if($insert !== null) {
			$insert->execute();	
		}
		
	}
	
	protected function _persistSizes(YahooApt_Ad $ad) {
		
		$insert = null;

		// remove all existing sizes
		$delete = db_delete('yahoo_apt_ad_size');
		$delete->condition('ad_id',$ad->getId());
		$delete->execute();
		
		foreach($ad->getSizes() as $item) {
			
			$data = array(
				'width'=> $item->width,
				'height'=> $item->height,
				'weight'=> $item->weight,
				'priority'=> $item->priority
			);
				
			if($insert === null) {
				$insert = db_insert('yahoo_apt_ad_size');
				$insert->fields(array('ad_id','width','height','weight','priority'));
			}
				
			$data['ad_id'] = $ad->getId();			
			$insert->values($data);
				
		}
		
		if($insert !== null) {
			$insert->execute();	
		}
		
	}
	
}