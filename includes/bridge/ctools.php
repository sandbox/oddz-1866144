<?php

/**
 * @implement of hook_ctools_plugin_directory
 */
function yahoo_apt_ctools_plugin_directory($module,$plugin) {
	if ($module == 'ctools' && $plugin == 'export_ui') {
    	return 'plugins/export_ui';
	}
}

/**
 * Implements hook_ctools_plugin_post_alter().
 * 
 * Modify yahoo_apt export_ui mapping plugins so that
 * when add and edit form is submited redirect occurs back to
 * custom datagrid rather than the default context list.
 */ 
function yahoo_apt_ctools_plugin_post_alter(&$plugin,$info) {
    
  if($info['module'] == 'ctools' && $info['type'] == 'export_ui') {
    switch($plugin['name']) {
      case 'yahoo_apt_path_mapping':
        $plugin['redirect']['add'] = 'admin/structure/yahoo-apt/mappings/paths';
        $plugin['redirect']['edit'] = 'admin/structure/yahoo-apt/mappings/paths';
        break;
                
      case 'yahoo_apt_term_mapping':
        $plugin['redirect']['add'] = 'admin/structure/yahoo-apt/mappings/terms';
        $plugin['redirect']['edit'] = 'admin/structure/yahoo-apt/mappings/terms';
        break;
        
      case 'yahoo_apt_default_mapping':
        $plugin['redirect']['add'] = 'admin/config/content/yahoo-apt';
        $plugin['redirect']['edit'] = 'admin/config/content/yahoo-apt';
        break;
                
      default:
    }
  }
    
}

/**
 * @implement hook_form_FORM_ID_alter
 */
function yahoo_apt_form_ctools_export_ui_edit_item_form_alter(&$form,&$form_state) {
	
	/**
	 * Ctools export ui form generated from internal callback for creating
	 * term or path mappings.
	 */
	if(isset($form_state['function args'][4]) && strpos($form_state['function args'][4],'yahoo_apt_') === 0) {
		
		// This is how we can separate term and path mappings for building lists
		$mapping_type = !empty($form_state['item']->tag)?$form_state['item']->tag:$form_state['function args'][4];
		
		/**
		 * Flag form as being used within the context of creating a context
		 * for a yahoo apt mapping.
		 */
		$form['#attributes']['class'][] = 'yahoo-apt-mapping';
		$form['#attributes']['class'][] = 'yahoo-apt-mapping-'.str_replace(array('yahoo_apt_','_mapping','_'),array('','','-'),$mapping_type);
		
		/**
		 * Title of page 
		 */
		if(strpos($mapping_type,'default') !== false) {
			drupal_set_title('Yahoo APT Settings');
		} else if(!empty($form_state['item']->name)) {
			drupal_set_title('Edit Mapping');
		} else {
			drupal_set_title('Add Mapping');
		}
		
		// A little dirty but gets the job done
		$form['info']['#attributes']['style'] = 'display: none;';
		
		$form['info']['name']['#access'] = false;
		$form['info']['name']['#type'] = 'value';
		
		/**
		 * When dealing with an existing mapping use that name. Otherwise, generate a new unique name for the mapping.
		 */
		$form['info']['name']['#value'] = !empty($form_state['item']->name)?$form_state['item']->name:yahoo_apt_generate_unique_mapping_name();
		
		/*
		 * IMPORTANT
		 * 
		 * Tagging has no other reponsibility besides being able to build separate
		 * lists of path and term mappings. NEVER use this to determine whether
		 * a context has a path or term mapping because the standard context form
		 * can still be used where the tag is a user supplied/controlled value/input.
		 */
		$form['info']['tag']['#access'] = false;
		$form['info']['tag']['#type'] = 'value';
		$form['info']['tag']['#value'] = $mapping_type;
		
		$form['info']['description']['#access'] = false;
		$form['condition_mode']['#access'] = false;
		
		/**
		 *  When creating a new mapping require all conditions. Otherwise use what has been specified. This is
		 *  critial when the domain context module is enabled so that the context is only applied to
		 *  the specified domains.  
		 */
		$form['condition_mode']['#value'] = !empty($form_state['item']->name)?$form_state['item']->condition_mode:1;
		
		//$form['conditions']['#title_display'] = 'invisible';
		$form['conditions']['selector']['#access'] = false;
		
		//$form['reactions']['#title_display'] = 'invisible';
		$form['reactions']['selector']['#access'] = false;
		$form['reactions']['selector']['#default_value'] = 'yahoo_apt_mapping';
		
		/**
		 * Change plugin based on type of mapping
		 */
		if(strpos($mapping_type,'term') !== false) {
			
			$form['conditions']['plugins']['node_taxonomy']['#context_enabled'] = true;
			$form['conditions']['selector']['#default_value'] = 'node_taxonomy';
			
			// require term
			$form['conditions']['plugins']['node_taxonomy']['values']['#required'] = true;
			
			if(module_exists('domaincontext')) {
				$form['conditions']['plugins']['domain']['#context_enabled'] = true;
			}
			
		} else if(strpos($mapping_type,'path') !== false) {
			
			$form['conditions']['plugins']['path']['#context_enabled'] = true;
			$form['conditions']['selector']['#default_value'] = 'path';
			
			// require path
			$form['conditions']['plugins']['path']['values']['#required'] = true;
			
			if(module_exists('domaincontext')) {
				$form['conditions']['plugins']['domain']['#context_enabled'] = true;
			}
			
		} else if(strpos($mapping_type,'default') !== false) {
			
			/**
			 * When domain context module is enabled a default context can be specified
			 * for every separate domain. Otherwise, the defualt applies to all domains.
			 */
			if(module_exists('domaincontext')) {
				$domain = domain_get_domain();
				$form['conditions']['plugins']['domain']['#context_enabled'] = true;
				$form['conditions']['plugins']['domain']['values']['#default_value'] = array($domain['domain_id']);
				unset($domain);
			} else {
				$form['conditions']['plugins']['sitewide']['#context_enabled'] = true;
				$form['conditions']['plugins']['sitewide']['values']['#default_value'] = array(1); 
			}
			
			/**
			 * Add pub id and site name fields
			 */
			$form += context_get_plugin('reaction','yahoo_apt_mapping')->settings_form();
			$form[YAHOO_APT_VAR_PUB_ID]['#weight'] = -1002;
			$form[YAHOO_APT_VAR_SITE_NAME]['#weight'] = -1001;
			
			/**
			 * Place web services credentials right below pub id and site name fields
			 */
			$form['web_service_credentials']['#weight'] = -1000;
			
			/**
			 * Custom submit handler for settings data
			 */
			$form['#submit'][] = 'yahoo_apt_mappings_defaults_submit';
			
		}
		
		/**
		 * Always enable yahoo apt reaction
		 */
		$form['reactions']['plugins']['yahoo_apt_mapping']['#context_enabled'] = true;
		
		/**
		 * Change path to a textfield
		 */
		$form['conditions']['plugins']['path']['values']['#type'] = 'textfield';
		
		/**
		 * Hide node taxonomy option for page.
		 */
		$form['conditions']['plugins']['node_taxonomy']['options']['node_form']['#access'] = false;
		
		//drupal_set_message('<pre>'.print_r($form,true).'</pre>');
		
		/**
		 * Hide delete button. Mappings should ONLY be deleted via the main listing data grid
		 * using the provided operations. This ensures that yahoo apt tables that rely on
		 * context are removed along with context. Deleting a context any other way will result
		 * in the context being removed without the associated tables in yahoo apt module. Which
		 * will not result in an error at this time because foreign keys are no enforced by Drupal
		 * but will leave orphan data laying around which is no good and/or if foreign keys are supported
		 * in some way in the future would result in a foreigh key constraint violation error.
		 */
		$form['buttons']['delete']['#access'] = false;
		
		/**
		 * When domain context module is enabled modify the description because current
		 * one is very misleading.
		 */
		if(module_exists('domaincontext')) {
			$form['conditions']['plugins']['domain']['values']['#description'] = 'Please specify the domains to activate mapping. When no domain is specified mapping will be active on ALL domains.';
		}
		
	}
	
	$form['#submit'][] = 'yahoo_apt_ctools_export_ui_edit_item_form_submit';
	
}

/**
 * Work around to get around the fact that there is no way to reference
 * a context on the context ui form from the reaction plugin options
 * submit handler. Instead of persiting the mapping in the plugin options submit
 * handler it will be done here. That way we have easy access to the context
 * item required for the foreign key.
 */
function yahoo_apt_ctools_export_ui_edit_item_form_submit(&$form,&$form_state) {
	
	$mapping = yahoo_apt_mapping_form_persist_mapping();
	
	/**
	 * Considering the export ui from can be used in many different
	 * conexts checking for the mapping is a surefire way to make
	 * certain that the export ui form is being used within the context
	 * of the context edit form. A mapping will not exist unless
	 * the context edit form has been submitted.
	 */
	if($mapping) {
	
		$mapping->setContext((object) array(
			'name'=> $form_state['item']->name
		));
		
		// Save mapping to db
		entity_get_controller('yahoo_apt_mapping')->persist($mapping);
	
	}
	
}

/**
 * Submit handler for having mapping defaults. This essentially takes
 * the pubid and site name using the proper save function based
 * on whether domain config has been enabled or not.
 */
function yahoo_apt_mappings_defaults_submit(&$form,&$form_state) {
	//drupal_set_message('save apt data');
	
	$pub_id = $form_state['values'][YAHOO_APT_VAR_PUB_ID];
	$site_name = $form_state['values'][YAHOO_APT_VAR_SITE_NAME];
	
	// web services authentication
	$username = $form_state['values'][YAHOO_APT_VAR_USERNAME];
	$password = $form_state['values'][YAHOO_APT_VAR_PASSWORD];
	$license = $form_state['values'][YAHOO_APT_VAR_LICENSE];
	$account_id = $form_state['values'][YAHOO_APT_VAR_ACCOUNT_ID];
	
	/**
	 * Optional domain configuration settings for supporting different
	 * pub and site names accross multiple site installation.
	 */
	if(module_exists('domain_conf')) {
		$domain = domain_get_domain();
		domain_conf_variable_set($domain['domain_id'],YAHOO_APT_VAR_PUB_ID,$pub_id);
		domain_conf_variable_set($domain['domain_id'],YAHOO_APT_VAR_SITE_NAME,$site_name);
	} else {
		variable_set(YAHOO_APT_VAR_PUB_ID,$pub_id);
		variable_set(YAHOO_APT_VAR_SITE_NAME,$site_name);
	}
	
	// variable set - same for every domain (web service variables)
	variable_set(YAHOO_APT_VAR_USERNAME,$username);
	variable_set(YAHOO_APT_VAR_PASSWORD,$password); // @todo: encode?
	variable_set(YAHOO_APT_VAR_LICENSE,$license);
	variable_set(YAHOO_APT_VAR_ACCOUNT_ID,$account_id);
	
}

/**
 * Save mapping to internal static variable to be persisted at a later
 * time. This function is called within the context reaction plugin
 * yahoo_apt_mapping inside the options_submit method. The mapping
 * is saved here so that it can than be retreived from 
 * yahoo_apt_ctools_export_ui_edit_item_form_submit where it is actually
 * persisted to the db with the proper context binding.
 * 
 * @param YahooApt_Mapping
 * @return YahooApt_Mapping
 */
function yahoo_apt_mapping_form_persist_mapping($mapping=null) {
	static $cache = null;
	
	if($mapping) {
		$cache = $mapping;
	} else {
		return $cache;
	}
}