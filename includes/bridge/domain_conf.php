<?php

/**
 * @implement hook_domain_conf
 */
function yahoo_apt_domain_conf() {
	
	$form = array();
	
	/** 
	 * Add pub id and site name settings. These will be configurable
	 * per domain.
	 */
	$form['yahoo_apt'] = array(
		'#type'=> 'fieldset',
		'#title'=> 'Yahoo APT Settings',
		'#collapsible'=> true,
		'#collapsed'=> true
	);
	
	$form['yahoo_apt']+= context_get_plugin('reaction','yahoo_apt_mapping')->settings_form();
	
	return $form;
	
}