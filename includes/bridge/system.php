<?php
/**
 * @implement hook_theme
 */
function yahoo_apt_theme() {
	return array(
		/**
		 * Theme is used to render the table with ad sizes inside
		 * the ad data entry admin form.
		 */
		'yahoo_apt_ad_form_ad_size_table'=> array(
			'render element'=> 'elements'
		),
		
		/**
		 * Theme adspot block
		 */
		'yahoo_apt_adcall'=> array(
			'variables'=>array('name'=>null,'enabled'=>true),
			'function'=> 'yahoo_apt_theme_adcall',
			'preprocess functions'=> array('yahoo_apt_preprocess_adcall')
		),
		
		/**
		 * Theme adspot for debugging
		 */
		'yahoo_apt_adcall_debug'=> array(
			'variables'=> array('name'=>null,'enabled'=>true,'ad'=>null,'mapping'=>null),
			'function'=> 'yahoo_apt_theme_adcall_debug',
			'preprocess functions'=> array('yahoo_apt_preprocess_adcall','yahoo_apt_preprocess_adcall_debug')
		)
	);
}

/**
 * @implement hook_element_info
 */
function yahoo_apt_element_info() {
	return array(
		/**
		 * Custom element is necessary to use a table
		 * inside the ad data entry form for managing ad sizes
		 * through the ad data entry form.
		 */
		'yahoo_apt_ad_form_ad_size_table'=> array(
			'#theme'=> 'yahoo_apt_ad_form_ad_size_table'
		)
	);
}

/**
 * @implement hook_page_build
 */
function yahoo_apt_page_build(&$page) {

	//drupal_set_message('yahoo_apt_page_build');
	//drupal_set_message('<pre>'.print_r(yahoo_apt_discover_context(),true).'</pre>');
	
	/**
	 * Execute yahoo_apt_plugin reaction
	 */
	$plugin = context_get_plugin('reaction', 'yahoo_apt_mapping');
  	if ($plugin) {
    	$plugin->execute();
  	}
	
	/**
	 * Add post render callback
	 * @see: http://drupal.org/node/1796228
	 */
	 $page['#post_render'][] = 'yahoo_apt_replace_ad_block_placeholders';
	 
	// advanced debug info
	if(yahoo_apt_debug_mode()) {
		yahoo_apt_page_debug_info();
	}
	
}

/**
 * When debugging is on add additional infromation to output to ease
 * process of determing why expected mapping settings are not being
 * applied. The main reason this would occur is because the mapping
 * was specified incorrectly or mutiple mappings exist where the most
 * specific one is chosen based on the rules defined by the mapping
 * disocovery function.
 */
function yahoo_apt_page_debug_info() {

	// mapping used for request
	$mapping = yahoo_apt_discover_mapping();
		
	// context used for request
	$context = yahoo_apt_discover_context();
	
	// actual adspots enabled on page
	$adspots = array();
	foreach(yahoo_apt_discover_adspots() as $adspot) {
		$adspots[] = $adspot->getName();
	}
		
	// ALL ad blocks for the page/request
	$blocks = array();
	foreach(yahoo_apt_ad_discovery() as $block) {
		// strike out blocks that are on the page but have have disabled adspots.
		$blocks[] = in_array($block,$adspots)?$block:"<s>$block</s>";
	}
	asort($blocks);
		
	// all active contexts for request
	$contexts = array();
	foreach(context_active_contexts() as $c) {
		$contexts[] = $c->name == $context->name?"<b>{$c->name}</b>":$c->name;
	}
		
	drupal_set_message("Mapping ID: {$mapping->getId()}");;
	drupal_set_message('Contexts: '.implode(',',$contexts));
	drupal_set_message('Ad Blocks: '.implode(',',$blocks));
	drupal_set_message('Advertising Disabled: '.(yahoo_apt_disable_advertising()?'<b>Yes</b>':'No'));

}

/**
 * Replace ad block placeholders. Theming blocks here is necessary
 * so that adspots can be rendered within panels. Otherwise, adspots
 * would not be rendered in panel panes because the execution order
 * changes. In particular hook_page_build is called after all the content
 * in a panel is converted to a string.
 */
function yahoo_apt_replace_ad_block_placeholders($children,$elements) {

	$match = array();
	$replace = array();
	
	$debug = yahoo_apt_debug_mode();
	
	/*
	 * Replace block placeholders with themed block content.
	 */
	foreach(yahoo_apt_ad_discovery() as $name) {
	
		$match[] = "<!-- yahoo-apt-ad-{$name}-block -->";
		
		if($debug) {
			$replace[] = theme('yahoo_apt_adcall_debug',array('name'=>$name,'enabled'=>true));
		} else {
			$replace[] = theme('yahoo_apt_adcall',array('name'=>$name,'enabled'=>true));
		}
		
	}
	
	return str_replace($match,$replace,$children);

}

/**
 * Preprocess ad call theme.
 */
function yahoo_apt_preprocess_adcall(&$vars) {

	// drupal_set_message('yahoo_apt_preprocess_adcall('.$vars['name'].')');
	
	/**
	 * Determine whether the ad should be visble or not. This is not the same thing
	 * as block visibility. The block will remain on the page but nothing will be rendered
	 * into it. This is really the "best" method I can think off for enabling/disabling
	 * ad blocks on specific pages.
	 */
	$plugin = context_get_plugin('reaction', 'yahoo_apt_mapping');
	if($plugin) {
		$vars['enabled'] = yahoo_apt_adspot_enabled($vars['name']);
	}
	
}

/**
 * Preprocess adcall debug theme
 */
function yahoo_apt_preprocess_adcall_debug(&$vars) {
	
	// load mapping data
	$mapping = yahoo_apt_discover_mapping();
	
	// assign ad
	$vars['ad'] = array_pop(entity_get_controller('yahoo_apt_ad')->load(null,array('ad'=>array('name'=>$vars['name']))));
	
	// assign mapping
	$vars['mapping'] = $mapping;
	
}

/**
 * Theme adcall block
 */
function yahoo_apt_theme_adcall($vars) {

	// drupal_set_message('yahoo_apt_theme_adcall('.$vars['name'].')');
	
	/**
	 * When advertising has not been disabled for the request and the adspot has been
	 * flagged as enabled display it.
	 */
	if(!yahoo_apt_disable_advertising() && $vars['enabled']) {
		return '<script type="text/javascript">yld_mgr.place_ad_here(\''.$vars['name'].'\');</script>';
	} else {
		return '';
	}
	
}

/**
 * Theme adcall for debug.
 */
function yahoo_apt_theme_adcall_debug($vars) {
	
	$ad = $vars['ad'];
	
	/**
	 * Use template helper to build out JS parameters for globally shared settings
	 */
	$shared_params = yahoo_apt_build_shared_parameters($vars['mapping']);
	
	/**
	 * Use template helper to build out JS ad parameters for single ad
	 */
	$ad_params = yahoo_apt_build_ad_parameters($vars['ad']);
	
	/**
	 * Build out HTML
	 */
	$html = '<div class="yahoo-apt-debug">';
	
	// name
	$html.= "<p><strong>ad spot {$ad->getName()}</strong></p>";
	
	// sizes
	$html.= '<p><strong>Sizes: </strong>'.implode(',',$ad_params->ad_size_list).'</p>';
	
	// placement
	$html.= "<p><strong>Placement: </strong>{$ad_params->ad_delivery_mode}</p>";
	
	// custom content categories
	$html.= '<p><strong>CCC: </strong>'.implode(',',$ad_params->cstm_content_cat_list).'</p>';
	
	// content types
	$html.= '<p><strong>Content Types: </strong>'.implode(',',$shared_params->content_type_list).'</p>';
	
	// content topics
	$html.= '<p><strong>Content Topics: </strong>'.implode(',',$shared_params->content_topic_id_list).'</p>';
	
	// custom sections
	$html.= '<p><strong>Custom Sections: </strong>'.implode(',',$shared_params->cstm_sctn_list).'</p>';
	
	// publication
	$html.= "<p><strong>Publication: </strong>{$shared_params->site_name}</p>";
	
	// enabled?
	$html.= '<p><strong>Enabled: </strong>'.($vars['enabled']?'Yes':'No').'</p>';
	
	$html.= '</div>';
	
	return $html;
	
}