<?php

/*
 * -----------------------------------------------------------------------------------------------------
 * Configuration settings
 * -----------------------------------------------------------------------------------------------------
 */

function yahoo_apt_settings_page() {
	//return 'yahoo apt settings';
	
	ctools_include('export-ui');
	
	/**
	 * Context flagged with this specifies the default settings.
	 */
	$tag = 'yahoo_apt_default';
	
	/**
	 * See if a default has been defined. If it has been defined load it and use it. Otherwise
	 * create new.
	 */
	$query = db_select('context','c');
	$query->fields('c',array('name','conditions'));
	$query->condition('c.tag',$tag);
	$result = $query->execute();
	
	/**
	 * Sites without domain context module installed will have settings applied to each domain. When
	 * domain context is installed each separate site can have a different default context.
	 */
	$row = null;
	if(module_exists('domaincontext')) {
		$domain = domain_get_domain();
		while($row=$result->fetchAssoc()) {
			$conditions = unserialize($row['conditions']);
			if(isset($domain['domain_id']) && in_array($domain['domain_id'],$conditions['domain']['values'])) {
				break;
			}
		}
		unset($domain);
	} else {
		$row = $result->fetchAssoc();
	}
	
	$context = null;
	if($row) {
		$context = context_load($row['name']);
	}
	
	//drupal_set_message('<pre>'.print_r($context,true).'</pre>');
	
	if($context) {
		return ctools_export_ui_switcher_page('yahoo_apt_default_mapping','edit',$context,null,$tag);
	} else {
		return ctools_export_ui_switcher_page('yahoo_apt_default_mapping','add',null,null,$tag);
	}
	
}