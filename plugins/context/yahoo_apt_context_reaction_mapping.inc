<?php
class yahoo_apt_context_reaction_mapping extends context_reaction {
	
	public function options_form($context) {	
		
		$values = $this->fetch_from_context($context);

		$mapping = null;		
		if($context->name) {
			$mapping = yahoo_apt_mapping_load_by_context($context->name);
		}
		
		if(!$mapping) {
			$mapping = new YahooApt_Mapping();
		}
		
		$form = array();
		
		/**
		 * Begin tabs
		 */
		$form['mapping'] = array(
			'#type'=> 'vertical_tabs',
			'#title'=> 'Yahoo APT Advertsing Mapping',
			'#default_tab'=> 'content_types_tab'
		);
		
		$form['mapping']['content_types_tab'] = array(
			'#type'=> 'fieldset',
			'#title'=> 'Content Types'
		);
		
		$content_types_flat = yahoo_apt_content_type_options();
		$content_types = yahoo_apt_options_to_hierarchy($content_types_flat);
		unset($content_types_flat);
		
		$form['mapping']['content_types_tab']['content_types'] = array(
			'#type'=> 'hierarchical_select',
			'#title'=> 'Content Types',
		    '#config' => array(
			  'module'=> 'hs_smallhierarchy',
		      'params' => array(
				'hierarchy'=> $content_types,
				'id' => 'yahoo-apt-content-types-hierarchy',
				'separator'=> '|'
			  ),
		      'save_lineage'    => 0,
		      'enforce_deepest' => 0,
		      'entity_count'    => 0,
		      'require_entity'  => 0,
		      'resizable'       => 0,
		      'level_labels' => array(
		        'status' => 0,
		        'labels' => array(),
		      ),
		      'dropbox' => array(
		        'status'   => 1,
		        'title'    => t('Active Content Types'),
		        'limit'    => 5, // YAHOO APT only allows up to 5
		        'reset_hs' => 1,
		      ),
		      'editability' => array(
		        'status'           => 0,
		        'item_types'       => array(),
		        'allowed_levels'   => array(),
		        'allow_new_levels' => 1,
		        'max_levels'       => 3,
		      ),
		      'render_flat_select'=> 1
		    ),
		    '#default_value' => array(),
		    '#description'=> 'Specifies the type of information on the page. You might also think of this as the page\'s function (such as Software Downloads or Ecommerce - Shopping). Maximum of 5 allowed.'
		);
		
		$form['mapping']['content_topics_tab'] = array(
			'#type'=> 'fieldset',
			'#title'=> 'Content Topics'
		);
		
		$content_topics_raw = yahoo_apt_content_topic_options();
		$content_topics = yahoo_apt_options_to_hierarchy($content_topics_raw);
		unset($content_topics_raw);
		
		$form['mapping']['content_topics_tab']['content_topics'] = array(
			'#type'=> 'hierarchical_select',
			'#title'=> 'Content Topics',
		    '#config' => array(
			  'module'=> 'hs_smallhierarchy',
		      'params' => array(
				'hierarchy'=> $content_topics,
				'id' => 'yahoo-apt-content-topics-hierarchy',
				'separator'=> '|'
			  ),
		      'save_lineage'    => 0,
		      'enforce_deepest' => 0,
		      'entity_count'    => 0,
		      'require_entity'  => 0,
		      'resizable'       => 0,
		      'level_labels' => array(
		        'status' => 0,
		        'labels' => array(),
		      ),
		      'dropbox' => array(
		        'status'   => 1,
		        'title'    => t('Active Content Topics'),
		        'limit'    => 10, // YAHOO APT only allows up to 10
		        'reset_hs' => 1,
		      ),
		      'editability' => array(
		        'status'           => 0,
		        'item_types'       => array(),
		        'allowed_levels'   => array(),
		        'allow_new_levels' => 1,
		        'max_levels'       => 3,
		      ),
		      'render_flat_select'=> 1
		    ),
		    '#default_value' => array(),
		    '#description'=> '<p>Use this parameter to select topics on which you\'d like to receive ads. As part of your initial publisher setup with your account manager, you\'ll specify whether you are using standard topics or publisher-specific topic IDs</p><ul><li>If you choose to use standard topics, your account manager can provide you with a list of topic IDs to use.</li><li>If you are using publisher-specific topics, your account manager will help you map them to Yahoo\'s standard taxonomy.</li></ul>'
		);
		
		$form['mapping']['custom_sections_tab'] = array(
			'#type'=> 'fieldset',
			'#title'=> 'Custom Sections'
		);
		
		$custom_sections = yahoo_apt_custom_section_options();
		$form['mapping']['custom_sections_tab']['custom_sections'] = array(
			'#type'=> 'hierarchical_select',
			'#title'=> 'Custom Sections',
		    '#config' => array(
			  'module'=> 'hs_flatlist',
		      'params' => array(
				'options'=> $custom_sections,
			  ),
		      'save_lineage'    => 0,
		      'enforce_deepest' => 1,
		      'entity_count'    => 0,
		      'require_entity'  => 0,
		      'resizable'       => 0,
		      'level_labels' => array(
		        'status' => 0,
		        'labels' => array(),
		      ),
		      'dropbox' => array(
		        'status'   => 1,
		        'title'    => t('Active Custom Sections'),
		        'limit'    => 10, // YAHOO APT only allows up to 10
		        'reset_hs' => 1,
		      ),
		      'editability' => array(
		        'status'           => 0,
		        'item_types'       => array(),
		        'allowed_levels'   => array(),
		        'allow_new_levels' => 1,
		        'max_levels'       => 3,
		      ),
		      'render_flat_select'=> 1
		    ),
		    '#default_value' => array(),
		    '#description'=> '<p>The Custom Section parameter allows you to set up custom values to indicate how a page or group of pages fits into the structure of your site &mdash; for example, LocalSports or EditorialColumn</p><p>The section represents the physical locale of the page within the hierarchy of your site.</p><p>This is an optional parameter; however, since advertisers sometimes specify the part of the site on which they want their ads to display, such as on the home page, specifying this information can help match up ads with your ad space.</p>'
		);
		
		// Less relevant options
		$form['mapping']['misc_tab'] = array(
			'#type'=> 'fieldset',
			'#title'=> 'Other Parameters'
		);
		
		$form['mapping']['misc_tab']['request_type'] = array(
			'#type'=> 'select',
			'#title'=> 'Request Type',
			'#default_value'=> $mapping->getRequestType(),
			'#options'=> array(
				'ac'=> 'Ad Call',
				'bc'=> 'Beacon Call',
				'fc'=> 'Forecast Call'
			),
			'#description'=> '<p>In addition to requesting ads, you can use the request type parameter to make a beacon call or a forecast call. There are three possible values for this parameter:<p> <ul><li><strong>Ad Call</strong> &mdash; the code bundle is requesting ads</li><li><strong>Beacon Call</strong> &mdash; the is still passed (e.g., for collecting user data) but no ads are displayed. You may choose to do this, for example, on a page with sensitive material.</li><li><strong>Forecast Call</strong> &mdash; the  is used for generating usage statistics only</li></ul>'
		);
		
		$form['mapping']['misc_tab']['click_destination'] = array(
			'#type'=> 'radios',
			'#title'=> 'Open Advertiser Page In: ',
			'#default_value'=> $mapping->getClickDestination(),
			'#options'=> array(
				'_blank'=> 'New Window',
				'_top'=> 'Same WIndow'
			),
			'#description'=> 'Specify whether clicks on ads will open advertiser links in same browser window or new browser window.', 
		);
		
		$form['mapping']['misc_tab']['disable_content_send'] = array(
			'#type'=> 'radios',
			'#title'=> 'Use Page Content To Increase Ad Relevence?',
			'#default_value'=> $mapping->getDisableContentSend(),
			'#options'=> array(
				0=> 'Yes',
				1=> 'No'
			),
			'#description'=> 'When disabled page content will not be included in ad request. Generally speaking it is best to include page content to increase ad relevence. However, including page content slightly increases page load time though Yahoo states this is imperceptible to the user.'
		);
		
		$form['mapping']['misc_tab']['exclude_rich_media'] = array(
			'#type'=> 'radios',
			'#title'=> 'Disable Rich Media?',
			'#default_value'=> $mapping->getExcludeRichMedia(),
			'#options'=> array(
				1=> 'Yes',
				0=> 'No'
			),
			'#description'=> 'Yahoo provides two ad formats: Standard Graphical and Rich Media. When rich media is not disabled both formats are fair game. When rich media is excluded only standard graphical ads will be shown.'
		);
		
		/**
		 * Ads enabled
		 */
		$form['mapping']['adspots_tab'] = array(
			'#type'=> 'fieldset',
			'#title'=> 'Active Adspots'
		);
		
		$form['mapping']['adspots_tab']['adspots'] = array(
			'#type'=> 'checkboxes',
			'#title'=> 'Activate Adspots',
			'#options'=> array(),
			'#default_value'=> array(),
			'#description'=> 'Check the adspots you would like to enable on the page. When no adspots have been checked all ad blocks on page will have ads served within them.'
		);
		
		$query = db_select('yahoo_apt_ad','a');
		$query->fields('a',array('id','name'));
		$result = $query->execute();
		
		while($row=$result->fetchAssoc()) {
			$form['mapping']['adspots_tab']['adspots']['#options'][$row['id']] = $row['name'];
		}
		
		/**
		 * Populate content topics, content types and custom sections.
		 * 
		 * This is a little tricky considering the default value must represent the full
		 * hierarchy in the format: A|A-1|B-2. Where | is used to separate each new level
		 * of the ancestory tree. The helper function aleviates some of this pain by accepting
		 * a converted hierarchical options array and finding the complete ancestory including
		 * the item itself by an items unique key or in this case an item value.
		 */
		foreach($mapping->getContentTypes() as $item) {
			$lineage = yahoo_apt_get_hierarchy_ancestory($content_types,$item->value);
			if($lineage) {
				$form['mapping']['content_types_tab']['content_types']['#default_value'][] = implode('|',$lineage);
			}
		}
		
		foreach($mapping->getContentTopics() as $item) {
			$lineage = yahoo_apt_get_hierarchy_ancestory($content_topics,$item->value);
			if($lineage) {
				$form['mapping']['content_topics_tab']['content_topics']['#default_value'][] = implode('|',$lineage);
			}
		}
		
		foreach($mapping->getCustomSections() as $item) {
			$form['mapping']['custom_sections_tab']['custom_sections']['#default_value'][] = $item->value;
		}
		
		/**
		 * Enabled adspots
		 */
		foreach($mapping->getAdspots() as $item) {
			$form['mapping']['adspots_tab']['adspots']['#default_value'][] = $item->ad_id;
		}
		
		/**
		 * This seems like the simplest method to persist
		 * the mapping id.
		 */
		if($mapping->getId()) {
			$form['mapping']['id'] = array(
				'#type'=> 'value',
				'#value'=> $mapping->getId()
			);
		}
		
		return $form;
	}
	
	/**
	 * @todo: reference context to get foriegn key
	 */
	public function options_form_submit($values) {
		
		// Create mapping
		$mapping = new YahooApt_Mapping();
		
		// When editing existing mapping set ID to trigger update
		if(isset($values['mapping']['id']) && !empty($values['mapping']['id'])) {
			$mapping->setId($values['mapping']['id']);
		}
		
		// Set atomic values
		$mapping->setRequestType($values['mapping']['misc_tab']['request_type']);
		$mapping->setClickDestination($values['mapping']['misc_tab']['click_destination']);
		$mapping->setDisableContentSend($values['mapping']['misc_tab']['disable_content_send']);
		$mapping->setExcludeRichMedia($values['mapping']['misc_tab']['exclude_rich_media']);
		
		/**
		 * Set content topics, content types and custom sections
		 * 
		 * The values will be represted as a lineage string with | as the separator
		 * for every level. However, we only need to the last item considering every
		 * ID is guanranteed to be unique throughout the entire tree. The lineage is not
		 * saved just the value representing the selected.
		 */
		foreach($values['mapping']['content_types_tab']['content_types'] as $value) {
		    $lineage = explode('|',$value);
			$mapping->addContentType((object) array('value'=>array_pop($lineage)));
		}
		
		foreach($values['mapping']['content_topics_tab']['content_topics'] as $value) {
		    $lineage = explode('|',$value);
			$mapping->addContentTopic((object) array('value'=>array_pop($lineage)));
		}
		
		foreach($values['mapping']['custom_sections_tab']['custom_sections'] as $value) {
			$mapping->addCustomSection((object) array('value'=>$value));
		}
		
		/**
		 * Add active adspots
		 */
		foreach($values['mapping']['adspots_tab']['adspots'] as $ad_id=>$enabled) {
			if(!empty($enabled)) {
				$mapping->addAdspot((object) array('ad_id'=>$ad_id));
			}
		}
		
		/**
		 * The mapping is saved using a static variable. Later a submit
		 * hook is used so that the actual context can be referenced. I don't really
		 * see any better way to get at the context name...
		 */
		yahoo_apt_mapping_form_persist_mapping($mapping);
		
		// mapping debug
		//drupal_set_message('<pre>'.print_r($mapping,true).'</pre>');
		
		return array('mapping'=>true);
		
	}
	
	public function settings_form() {
		
		$form = array(
			YAHOO_APT_VAR_PUB_ID=> array(
				'#type'=> 'textfield',
				'#title'=> 'Pub ID',
				'#maxlength'=> 128,
				'#default_value'=> variable_get(YAHOO_APT_VAR_PUB_ID, '')
			),
			YAHOO_APT_VAR_SITE_NAME=> array(
				'#type'=> 'textfield',
				'#title'=> 'Site Name',
				'#maxlength'=> 128,
				'#default_value'=> variable_get(YAHOO_APT_VAR_SITE_NAME, '')
			)
		);	
		
		/**
		 * Web services authentication credentials.
		 *
		 * web service use has not been implemented. It would be nice but I don't
		 * think it is really practical at this point.
		 */
		$form['web_service_credentials'] = array(
			'#access'=> false,
			'#type'=> 'fieldset',
			'#title'=> 'Web Service Credentials',
			'#collapsed'=> false,
			'#collapsible'=> true,
			'#description'=> 'These credentials are required for generating the options for custom content categories, custom sections and reporting tags. All of which will be dynamically generated based on the values which have been defined in the external yahoo apt ad manager interface/admin. These settings will be shared accross all domains in the multiple site installation using domain access.'
		);
		
		$form['web_service_credentials'][YAHOO_APT_VAR_USERNAME] = array(
			'#type'=> 'textfield',
			'#title'=> 'Username',
			'#default_value'=> variable_get(YAHOO_APT_VAR_USERNAME, '')
		);
		
		$form['web_service_credentials'][YAHOO_APT_VAR_PASSWORD] = array(
			'#type'=> 'textfield',
			'#title'=> 'Password',
			'#default_value'=> variable_get(YAHOO_APT_VAR_PASSWORD, '')
		);
		
		$form['web_service_credentials'][YAHOO_APT_VAR_LICENSE] = array(
			'#type'=> 'textfield',
			'#title'=> 'License Key',
			'#default_value'=> variable_get(YAHOO_APT_VAR_LICENSE, '')
		);
		
		$form['web_service_credentials'][YAHOO_APT_VAR_ACCOUNT_ID] = array(
			'#type'=> 'textfield',
			'#title'=> 'Account ID',
			'#default_value'=> variable_get(YAHOO_APT_VAR_ACCOUNT_ID, '')
		);
		
		return $form;
	}
	
	/**
	 * Modify page state based on first active context. Please note
	 * that when a request matches multiple contexts ONLY the first one
	 * will be used. This module does not support merging multiple contexts
	 * into one for many reasons but really because it would result in some
	 * very difficult to read code and painful debugging. Instead the approach
	 * has been kept as simple stupid as possible using only the first context
	 * has one a simple algorithm to determine context with highest precedence.
	 */
	public function execute() {
		
		/**
		 * Bail out when advertising is disabled for the request.
		 */
		if(yahoo_apt_disable_advertising()) {
			return;
		}
		
		// list of ads in request
		$ads = yahoo_apt_discover_adspots();
		
		/**
		 * Bail out asap when no ads have been detected
		 * on the page.
		 */
		if(empty($ads)) {
			return;
		}
		
		// get context
		$context = yahoo_apt_discover_context();
		
		// get mepping
		$mapping = yahoo_apt_discover_mapping();
		
		// Convert mapping to APT shared parameters format
		$params = yahoo_apt_build_shared_parameters($mapping);
		
		// Convert individual ads to APT per ad format when they are enabled on the page
		$ad_params = array();
		foreach($ads as $ad) {
			$ad_params[$ad->getName()] = yahoo_apt_build_ad_parameters($ad);
		}
		
		/**
		 * Use this info to determine which context and mapping is active
		 * for the request. This is much less relevant with the advent of the
		 * inbuilt debugger. None the less, it will be left intact for the time being.
		 */
		if(!$context) {
			$js = "/* context: null , mapping: null (use defaults) */".PHP_EOL;
		} else {
			$js = "/* context: '{$context->name}' , mapping: {$mapping->getId()} */".PHP_EOL;
		}
		
		/**
		 * Build out global JS
		 */
		$js.= yahoo_apt_convert_params_to_js($params);
		
		// Ad slots
		$js.= 'yld_mgr.slots = {};'.PHP_EOL;
		foreach($ad_params as $slot=>$ad) {
			$js.= yahoo_apt_convert_params_to_js($ad,'yld_mgr.slots["'.$slot.'"]');
		}
		
		// Ad call configuration
		drupal_add_js($js,array(
			'type'=>'inline',
			'group'=> JS_DEFAULT
		));
		
		// Yahoo add bundle code
		// @todo: expose as configuration because I believe it was stated in the docs that is can be different though is not for morris...
		drupal_add_js('http://e.yieldmanager.net/script.js',array(
			'type'=>'external',
			'group'=> JS_THEME
		));
		
	}
	
}